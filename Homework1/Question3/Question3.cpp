/* 
 * File:   Question3.cpp
 * Author: user
 *
 * Created on September 5, 2021, 8:03 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main() {
    
    double subTotal = 88.67; // Variable for subTotal
    double taxRate = .0675; // Variable for taxRate
    double tip = .2; // Variable for tip
    double taxAmount; // Variable for taxAmount
    double tipAmount; // Variable for tipAmount
    double taxTotal; //Variable for taxTotal
    double totalBill; // Variable for totalBill
    
    taxAmount = subTotal * taxRate; // Calculates taxAmount
    taxTotal = subTotal + taxAmount; // Calculates taxTotal
    tipAmount = taxTotal * tip; // Calculates tipAmount
    totalBill = taxTotal + tipAmount; // Calculates totalBill
    
    cout << "Meal Cost: " << subTotal << endl; // Outputs Meal cost
    cout << "Tax: " << taxAmount << endl; // Output tax
    cout << "Tip: " << tipAmount << endl; // Output Tip
    cout << "Total Bill: " << totalBill << endl; // Outputs calculated totalBill 
    

    return 0;
}

