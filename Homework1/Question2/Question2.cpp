/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main() {
    
    double purchase = 95;
    double stateTax = .04;
    double countyTax = .02;
    double totalTax;
    double totalSale;
    
    totalTax = stateTax + countyTax;
    totalSale = purchase * (1 + totalTax);
    
    cout << totalSale << endl;
    
    return 0;
}