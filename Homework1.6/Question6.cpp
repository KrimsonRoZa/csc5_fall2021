/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Question6.cpp
 * Author: Karina
 *
 * Created on September 13, 2021, 1:14 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

   int number_of_pods, peas_per_pod, total_peas;
    
   cout << "Hello\n"; // Added for question 2
   
   
    cout << "Press return after entering a number. \n";
    cout << "Enter the number of pods: \n";
    
    cin >> number_of_pods;
    
    cout << "Enter the number of peas in a pod: \n";
    cin >> peas_per_pod;
    total_peas = number_of_pods + peas_per_pod; // Changed from * to / for Question 3 ; changed  * to + for Question 4
    cout << "If you have ";
    cout << number_of_pods;
    cout << "pea pods \n";
    cout << "and ";
    cout << peas_per_pod;
    cout << " peas in each pod, then\n";
    cout << "you have ";
    cout << total_peas;
    cout << " peas in all the pods. \n"; 
    
    cout << "Goodbye \n"; // Added for question 2
    
    return 0;
}

