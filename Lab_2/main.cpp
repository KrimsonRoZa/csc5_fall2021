/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.cpp
 * Author: Karina
 *
 * Created on September 4, 2021, 10:39 AM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main() {

    int s1;
    int s2;
    int s3;
    double t;
    
    cout << "Enter your 3 most recent test scores in any order!" << endl;
    cin >> s1;
    cin >> s2;
    cin >> s3;
    t = (s1 + s2 + s3) / 3.0;
    
    cout << "The average of your test scores is: " << t << endl;
    
    return 0;
}

