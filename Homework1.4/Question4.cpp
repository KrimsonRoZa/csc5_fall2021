/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Question4.cpp
 * Author: Karina
 *
 * Created on September 12, 2021, 10:59 PM
 */

#include <cstdlib>
#include<iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int gallonsOfGas = 15; // Setting the total gallons the car can hold
    int milesDriven = 375; // Setting the number of miles driven 
    int milesPerGallon;  // Setting a variable for the calculated MPG 
    
    
    milesPerGallon = (milesDriven/gallonsOfGas ); // Calculate the number of miles per gallon
    
    cout << milesPerGallon; // Output the calculated value for MPG 

    return 0;
}

